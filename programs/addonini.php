;<?php/*

[general]
name							="theme_shopadmin"
version							="0.0.4"
addon_type						="THEME"
encoding						="ISO-8859-15"
mysql_character_set_database	="latin1,utf8"
description						="Theme for online shop administration"
description.fr					="Theme du back-office de gestion de la boutique en ligne"
delete							=1
ov_version						="8.1.98"
php_version						="5.1.0"
addon_access_control			="0"
author							="Cantico"
icon							="Retail-Shop.png"
mysql_character_set_database	="latin1,utf8"


; */?>
